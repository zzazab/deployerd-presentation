{{ $name := "phantomjs_reklama"}}
[[components.container]]
[components.container.params]
  name = "{{$name}}"
  pool = "{{.Params.pool}}"
  images = ["phantomjs-ubuntu"]

[[components.finalizer]]
[components.finalizer.params]
  name = "finalizer"
