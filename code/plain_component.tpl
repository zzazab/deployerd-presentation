[resource]
  images = [{{range $index, $image := .Params.images -}} "{{$image}}", {{- end}}]
  id = "{{.Params.name}}.in.ngs.ru"
  networks = ["br0"]
  pool = "{{.Params.pool}}"
  key_file = "/var/lib/deployer/creds/zone1.pub"
  need_start = {{.Params.need_start}}
  can_keep = {{.Params.can_keep}}
  add_sphinx_images = {{.Params.sphinx_images}}

[providers.pdns.roles]
  all = ["{{.Params.name}}"]
  {{range .Params.dns_roles}}
  {{.}} = ["{{$.Params.name}}"]
  {{end}}
# vim: ft=toml ts=2 sts=2 sw=2
