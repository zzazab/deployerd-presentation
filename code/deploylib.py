# coding=utf-8
import subprocess
import os
import time
import sys
import json
import xmlrpclib
import socket
import requests
import string
import traceback
import OpenSSL.crypto as crypto
import heaver.client as client
import re
import MySQLdb

from bg_alloc import BackgroundDbAllocator
from bg_alloc import TimeoutTransport
from powerdns import PowerDns
from operator import itemgetter

# ВНИМАНИЕ! Жесть какой лапша-код! Уберите женщин и детей от монитора!
# ATTENTION! Braindamaging code! Does not acceptable for woman and kids!

# globals
#FIXME: we should add key file to 'zone' variable
zone_private = "/var/lib/deployer/creds/Kt.+157+14128.private"

def get_host_from_farm(dbfarm_endpoint):
    return socket.gethostbyname(dbfarm_endpoint.split(":")[1].strip("/"))


def resolve_dns_name(name):
    octets = name.split(".")
    if len(octets) == 4:
        try:
            nums = map(int, octets)
            if all(lambda octet: 0 <= octet <= 255, nums):
                # is an ip
                return name
        except:
            pass

    return socket.gethostbyname(name)


def get_database(lb, db_type, jid, env):
    h = {"content-type": "application/json"}
    pl = {'op': 'get',
          'params': {
              "env_owner": jid[0],
              "env_domain": env}
          }
    url = lb + "/api/v1/instances/" + db_type

    try:
        r = requests.patch(url, data=json.dumps(pl), headers=h)
    except requests.ConnectionError:
        raise Exception("Loadbalancer unavaliable")

    if r.status_code != 200:
        raise Exception("Something wrong with loadbalancer")

    try:
        instance = r.json()
    except ValueError as err:
        print("Error decoding responce!\n Responce: %s" % r.text)
        raise err

    if instance["type"] != "INFO":
        raise Exception("Error while creating {0} db: {2}".format(db_type, instance["message"]))

    instance = instance["message"]

    db_id = instance[0]
    instance = instance[1]
    if db_type == "mysql":
        roleids = { "dbread": 1, "dbwrite": 1}
        run_list = ["dbread", "dbwrite"]
    if db_type == "mongo":
        roleids = {"mongodb": 1}
        run_list = ["mongodb"]

    ipaddr = resolve_dns_name(instance["host"])
    farmed = {"hostname": "stub-%s" % db_type,
              "roleids": roleids,
              "run_list": run_list,
              "ipaddr": ipaddr,
              "meta": { "port": instance["port"] }}
    inst   = {"type": db_type,
              "ipaddr": ipaddr,
              "port": instance["port"],
              "id": db_id}

    return farmed, inst


def drop_database(lb, db_id, db_ip):
    h = {"content-type": "application/json"}
    pl = {'op': 'free',
          'params': {
              "host": db_ip
          }}
    url = lb + "/api/v1/instance/" + db_id

    try:
        r = requests.delete(url, data=json.dumps(pl), headers=h)
    except requests.ConnectionError:
        raise Exception("Loadbalancer unavaliable")

    if r.status_code != 200:
        raise Exception("Something wrong with loadbalancer")

    try:
        instance = r.json()
    except ValueError as err:
        print("Error decoding responce!\n Responce: %s" % r.text)
        raise err

    if instance["type"] != "INFO":
        if instance["message"] == "Instance with id - %s not found" % db_id:
            print "Db {0} not found on farm {1}".format(db_id, db_ip)
            return
        raise Exception("Error while deleting db {0} from {1}: {2}".format(db_id, db_ip, instance["message"]))

    print "Dropped db {0} from farm {1}".format(db_id, db_ip)


def get_farm_for_db(host, farms):
    ip = resolve_dns_name(host)
    for farm in farms:
        if ip == get_host_from_farm(farm):
            return farm

    return None

def find_farmed_db_by_attr(dbfarm_endpoint, attr):
    api = xmlrpclib.ServerProxy(dbfarm_endpoint,transport=TimeoutTransport())
    name, value = attr
    try:
        resp = api.list()
    except socket.timeout:
        print("Can not obtain db list from dbfarm {0}".format(dbfarm_endpoint))

    try:
        resp = json.loads(resp)
        if resp['type'] == "INFO":
            bases = resp['message']
        else:
            bases = []
    except TypeError:
        bases = resp
    except IndexError:
        raise Exception("Db_farm version {0} not implemented")
    except socket.error:
        print("Can not obtain db list from dbfarm {0}".format(dbfarm_endpoint))
        return None

    result = filter(lambda inst: inst[1].get(name) == value, bases.items())
    if len(result) == 0:
        return None
    else:
        return result[0]


def call(command, stdin=""):
    return subprocess.Popen(command, stdout=subprocess.PIPE, env=os.environ, shell=True).communicate(stdin)[0]


def query_knife(command, stdin = ""):
    env = os.environ
    #FIXME: there is '-n' flag for knife, which allow passing data without editor,
    # but it don't work
    env["EDITOR"] = "tee"
    return subprocess.Popen("knife %s " % command, stdout = subprocess.PIPE, stdin = subprocess.PIPE, env = env, shell = True).communicate(stdin)


def query_databag(command, stdin = ""):
    env = os.environ
    env["EDITOR"] = "tee"
    return subprocess.Popen("knife data bag %s" % command, stdout = subprocess.PIPE, stdin = subprocess.PIPE, env = env, shell = True).communicate(stdin)


def get_nova_network_address(instance_name, network_name):
    instance_info = call("nova show %s" % instance_name).split("\n")
    target_line = [ line for line in instance_info if line.find(network_name) != -1][0]
    return target_line.split("|")[2].strip(" ")


def list_instances():
    while True:
        out = call("euca-describe-instances")
        if out.find("An unknown error has occurred") < 0:
            insts = []

            for line in out.split("\n"):
                if len(line) > 0 and line.find("INSTANCE") == 0:
                    insts.append(line.split("\t"))

            return insts


def get_balanced_compute_host(hosts, rl):
    insts = list_instances()

    # just get a dict with "hostname":0 values
    total_ves = dict(map(lambda item: (item[0], 0), hosts.items()))
    host_names = hosts.keys()

    inst_hosts = map(lambda line: line[6].strip(")").rpartition(" ")[2], insts)

    # get count of running ve's on each compute host
    for inst_host in inst_hosts:
        if inst_host in host_names:
            total_ves[inst_host] += 1

    # calculate load rating
    host_rating = hosts.copy()

    # algorithm is simple: host original rating is memory.
    # Every container takes 4 units of memory
    for host_name, ve_count in total_ves.items():
        host_rating[host_name] -= ve_count * 4

    # get host with largest rating
    target = reduce(lambda prev, cur: prev[1] > cur[1] and prev or cur,
                    host_rating.items(), [None, None])

    return target[0]


def spawn_ve(spawn_host, key, instance_type, ami):
    #out = call("euca-run-instances -n %d -z %s -k %s -t %s %s" % (total - special_ve_count, nova_zone, key, instance_type, ami))
    out = call("euca-run-instances -z %s -k %s -t %s %s" % (
                spawn_host, key, instance_type, ami))
    for line in out.split('\n')[:-1]:
        fields = line.split('\t')
        if fields[0] == "INSTANCE":
            return fields[1]


def allocate_nova_volume(size, zone):
    volume_name = call("euca-create-volume -s %(size)s -z %(zone)s" % locals()).split("\t")[1]
    #vol_idlog.write("%s\n" % volume_name)

    # wait for allocation
    while True:
        status = call("euca-describe-volumes %(volume_name)s" % locals()).split("\t")[5].split(" ")[0]

        if status == "creating":
            # that's ok, just wait a little
            time.sleep(1)
        elif status == "available":
            # done
            break
        else:
            print "invalid status: %s"  % status

    return volume_name


def attach_nova_volume(instance, volume):
    call("euca-attach-volume -d /dev/vdc -i %(instance)s %(volume)s" % locals())


def sanitize_env_id(env_id):
    allowed_chars = string.ascii_letters + string.digits + "-"
    sanitized_env_id = ""

    for char in env_id:
        if char in allowed_chars:
            sanitized_env_id += char
        else:
            sanitized_env_id += "-"

    return sanitized_env_id


#FIXME: хак для ENVIRONMENT-404
def get_ami(spawn_host, config, args, prev_ami):
    if config["type"] in ("func", "dev"):
        if (not args.skip_sphinx_cache
            and config["compute-nodes"][spawn_host]["volume_type"] == "image"):
            return config["env"][config["type"]]["ami"]["sphinxed"]
    return prev_ami


def make_ssl_cert(config):
    "Генерирует сертификат для *.ngs.ru.$domain, подписывает нашим CA"
    cacert_file = open(os.path.join(config["deployer_root"], "ssl", "cacert.pem"))
    cacert = crypto.load_certificate(crypto.FILETYPE_PEM, cacert_file.read())
    cacert_file.close()

    cakey_file = open(os.path.join(config["deployer_root"], "ssl", "cacert.key"))
    cakey = crypto.load_privatekey(crypto.FILETYPE_PEM, cakey_file.read(), str(config["cakey_password"]))
    cakey_file.close()

    key = crypto.PKey()
    key.generate_key(crypto.TYPE_RSA, 1024)

    cert = crypto.X509()
    subj = cert.get_subject()
    subj.CN = "*.ngs.ru.%s" % config["domain"]
    subj.C = "RU"
    subj.ST = "Province of Novosibirsk"
    subj.L = "Novosibirsk"
    subj.O = "NGS"
    subj.OU = "OAPP"
    subj.emailAddress = "oapp@office.ngs.ru"

    cert.set_serial_number(config["ssl_serial"])
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(60 * 60 * 24 * 3560) # 10 лет
    cert.set_issuer(cacert.get_subject())
    cert.set_pubkey(key)
    cert.sign(cakey, "sha1")

    cert_text = crypto.dump_certificate(crypto.FILETYPE_PEM, cert)
    key_text = crypto.dump_privatekey(crypto.FILETYPE_PEM, key)
    return (cert_text, key_text)


def deploy(args=None, config={}):

    status = {"dbinfo": [], "hosts_all": {}, "volinfo": [], "error": None}

    mysql_inst = None
    mongo_inst = None
    mysql_done = False
    mongo_done = False

    # generic config

    role_instance_types = {}
    try:
        # config for loadtesting environment! ###################################
        if config["type"] == "load": # type load
            # node count for lt [useful]
            webnodes     = 2
            #memcaches     = 1
            #dbreads     = 1


            # ami of image to use [useful]
            ami         = config["env"][config["type"]]["ami"]["default"]
            # instance resources [useful]
            instance_type    = "m1.medium"

            # network name, when frontends reside [useful, static]
            frontend_network_name = "testing-net2"
            frontend_network_device = "eth1"

            backend_network_device = "eth0"

            # ssh key in nova [useful]
            key         = config["ssh_key_name"]
            # ssh client key location [useful]
            key_path    = os.path.join(config["env_creds_dir"], key)


            # cookbooks. All local cookbooks will be uploaded now [useful for dev]
            #cookbooks_path     = os.path.join(os.environ["HOME"], "repos/chef/cookbooks")
            #cookbooks     = [dir for dir in os.listdir(cookbooks_path) if os.path.isdir(os.path.join(cookbooks_path, dir))]
            cookbooks = filter(lambda line: len(line.strip()) > 0,
                               open("/".join((config["deployer_root"], "cookbook_list"))).read().split())

            # log path
            #log_dir        = os.path.join(os.environ["HOME"], ".deployer", "logs")
            log_dir        = config["env_log_dir"]

            # resolver ip [static]
            resolver    = config["resolver"]

            # sphinx_cache [static]
            sphinx_cache_ipaddr    = "dbfarm.ngs.local"

            # current environment id [useful]
            env_id        = config["env_id"]

            # is it dev environment? [not used yet]
            env_is_devel    = False

            # root domain zone of all environments [now useful]
            root_zone    = config["root_domain"]
            # domain zone for current environment [should stay the same]
            env_zone    = config["domain"]

            # rolenames, used in loadtesting deploy [static]
            rolenames = ['webnode', 'cachenode', 'phpnode', 'appnode',
                         'memcached', 'search',
                         'mongodb', 'redis', 'task', 'loadbalancer', 'loadbalancer-haproxy',
                         'storage', 'video-server', 'static', 'updater', 'dbwrite', 'dbread']

            # run list for loadtesting spec [static]
            #run_lists =    [["loadbalancer"]] * 2 + \
            #        [["webnode", "phpnode", "cachenode", "storage", "static"]] * (webnodes - 1) + \
            #        [["search", "appnode", "redis"]] + \
            #        [["webnode", "phpnode", "storage", "static", "updater", "task"]] + \
            #        [["appnode", "video-server"]] + \
            #        [["memcached"]] * memcaches + \
            #        [["dbwrite"]] + \
            #        [["dbread"]] * dbreads + \
            #        [["mongodb"]] + \
            #        []

            run_lists = [["loadbalancer"]] + \
                    [["loadbalancer-haproxy"]] + \
                    [["appnode", "webnode", "cachenode"]] * 2 + \
                    [["phpnode", "memcached", "redis"]] + \
                    [["appnode", "search", "task", "video-server", "storage", "static", "updater"]] + \
                    [["mongodb"]] + \
                    []

            print "start farming mysql..."
            if args.dbwrite and args.dbread:
                dbwrite_host, dbwrite_port = args.dbwrite[0].split(":")
                dbread_host, dbread_port = args.dbread[0].split(":")
                dbwrite_farmed = {"ipaddr": dbwrite_host, "meta": {"port": dbwrite_port}, "run_list": ["dbwrite"], "roleids": {"dbwrite": 1}, "hostname": "dbwrite"}
                dbread_farmed = {"ipaddr": dbread_host, "meta": {"port": dbread_port}, "run_list": ["dbread"], "roleids": {"dbread": 1}, "hostname": "dbread"}



            volume_demand_map = {
                "mongodb": {"size": 200, "mountpoint": "/data/mongodb"},
                "search": {"size": 30, "mountpoint": "/data/soft/sphinx/var"}
            }

            role_instance_types = {
                "phpnode": "m1.custom_php",
                "webnode": "m1.medium",
                "search": "m1.medium",
                }


            # comment for volumes
            #volume_demand_map = {}

        elif config["type"] == "realty": # type realty
            # node count for lt [useful]
            webnodes    = 1
            phpnodes    = 2

            # ami of image to use [useful]
            ami         = config["env"][config["type"]]["ami"]["default"]
            # instance resources [useful]
            instance_type    = "m1.medium"

            # network name, when frontends reside [useful, static]
            frontend_network_name = "testing-net2"
            frontend_network_device = "eth1"

            backend_network_device = "eth0"

            # ssh key in nova [useful]
            key         = config["ssh_key_name"]
            # ssh client key location [useful]
            key_path    = os.path.join(config["env_creds_dir"], key)


            # cookbooks. All local cookbooks will be uploaded now [useful for dev]
            #cookbooks_path     = os.path.join(os.environ["HOME"], "repos/chef/cookbooks")
            #cookbooks     = [dir for dir in os.listdir(cookbooks_path) if os.path.isdir(os.path.join(cookbooks_path, dir))]
            cookbooks = filter(lambda line: len(line.strip()) > 0,
                               open("/".join((config["deployer_root"], "cookbook_list"))).read().split())

            # log path
            #log_dir        = os.path.join(os.environ["HOME"], ".deployer", "logs")
            log_dir        = config["env_log_dir"]

            # resolver ip [static]
            resolver    = config["resolver"]

            # sphinx_cache [static]
            sphinx_cache_ipaddr    = "dbfarm.ngs.local"

            # current environment id [useful]
            env_id        = config["env_id"]

            # is it dev environment? [not used yet]
            env_is_devel    = False

            # root domain zone of all environments [now useful]
            root_zone    = config["root_domain"]
            # domain zone for current environment [should stay the same]
            env_zone    = config["domain"]



            # rolenames, used in loadtesting deploy [static]
            rolenames = ['webnode', 'phpnode', 'cachenode', 'appnode',
                         'memcached', 'search', 'dbwrite', 'dbread',
                         'mongodb', 'redis', 'task', 'loadbalancer', 'loadbalancer-haproxy',
                         'storage', 'video-server', 'static', 'updater']

            # run list for loadtesting spec [static]
            #run_lists =    [["loadbalancer"]] * 2 + \
            #        [["webnode", "appnode", "cachenode", "storage", "static"]] * webnodes + \
            #        [["search", "appnode", "redis", "mongodb", "video-server"]] + \
            #        [["webnode", "phpnode", "memcached"]] * phpnodes + \
            #        [["updater", "appnode", "memcached", "task"]] + \
            #        [["dbwrite"]] + \
            #        [["dbread"]] + \
            #        []
            run_lists = [["loadbalancer"]] + \
                    [["loadbalancer-haproxy"]] + \
                    [["webnode", "cachenode"]] + \
                    [["search", "appnode"]] + \
                    [["phpnode", "memcached"]] + \
                    [["phpnode", "redis" ]] + \
            [["storage", "static", "appnode", "updater"]] + \
                    [["appnode", "video-server"]] + \
                    [["appnode", "task"]]  + \
                    [["dbwrite"]] + \
                    [["dbread"]] + \
                    [["mongodb"]] + \
                    []

            volume_demand_map = {
                "dbwrite": {"size": 250, "mountpoint": "/var/lib/mysql"},
                "dbread": {"size": 250, "mountpoint": "/var/lib/mysql"},
                "mongodb": {"size": 100, "mountpoint": "/data/mongodb"},
                "search": {"size": 50, "mountpoint": "/data/soft/sphinx/var"}
            }


            # comment for volumes
            #volume_demand_map = {}

        elif config["type"] == "storage": # type storage
            # node count for lt [useful]
            webnodes    = 2
            phpnodes    = 2
            cachenodes  = 2
            tasks       = 2

            # ami of image to use [useful]
            ami         = config["env"][config["type"]]["ami"]["default"]
            # instance resources [useful]
            instance_type    = "m1.tiny"

            # network name, when frontends reside [useful, static]
            frontend_network_name = "testing-net2"
            frontend_network_device = "eth1"

            backend_network_device = "eth0"

            # ssh key in nova [useful]
            key         = config["ssh_key_name"]

            # ssh client key location [useful]
            key_path    = os.path.join(config["env_creds_dir"], key)


            # cookbooks. All local cookbooks will be uploaded now [useful for dev]
            #cookbooks_path     = os.path.join(os.environ["HOME"], "repos/chef/cookbooks")
            #cookbooks     = [dir for dir in os.listdir(cookbooks_path) if os.path.isdir(os.path.join(cookbooks_path, dir))]
            cookbooks = filter(lambda line: len(line.strip()) > 0,
                               open("/".join((config["deployer_root"], "cookbook_list"))).read().split())

            # log path
            #log_dir        = os.path.join(os.environ["HOME"], ".deployer", "logs")
            log_dir        = config["env_log_dir"]

            # resolver ip [static]
            resolver    = config["resolver"]

            # sphinx_cache [static]
            sphinx_cache_ipaddr    = "dbfarm.ngs.local"

            # current environment id [useful]
            env_id        = config["env_id"]

            # is it dev environment? [useful]
            env_is_devel    = False

            # root domain zone of all environments [now useful]
            root_zone    = config["root_domain"]
            # domain zone for current environment [should stay the same]
            env_zone    = config["domain"]



            # rolenames, used in storage env deploy [static]
            rolenames = ['webnode', 'phpnode', 'cachenode', 'appnode',
                         'memcached', 'search', 'dbwrite', 'dbread',
                         'mongodb', 'redis', 'task', 'loadbalancer', 'loadbalancer-haproxy',
                         'storage', 'video-server', 'static', 'updater']

            # run list for loadtesting spec [static]
            run_lists =    [["loadbalancer"]]  + \
                    [["loadbalancer-haproxy"]] + \
                    [["appnode", "storage", "static"]] * webnodes + \
                    [["cachenode", "video-server"]] * cachenodes + \
                    [["search", "appnode", "redis",
                      "mongodb"]] + \
                    [["webnode", "phpnode", "memcached"]] * phpnodes + \
                    [["updater", "appnode", "memcached", "task"]] + \
                    [["appnode", "task"]] * (tasks - 1) + \
                    [["dbwrite"]] + \
                    [["dbread"]] + \
                    []

            volume_demand_map = {
                "dbwrite": {"size": 15, "mountpoint": "/var/lib/mysql"},
                "dbread": {"size": 15, "mountpoint": "/var/lib/mysql"},
                # "mongodb": {"size": 100, "mountpoint": "/data/mongodb"},
                "search": {"size": 45, "mountpoint": "/data/soft/sphinx/var"},
            }

            role_instance_types = {
                "webnode": "m1.medium",
                "search": "m1.medium",
                "dbread": "m1.small",
                "dbwrite": "m1.small",
            }

            # comment for volumes
            #volume_demand_map = {}

        # config for FUNCTIONAL testing environment! ##############################################
        elif config["type"] in ("func", "dev"): # type func

            ami         = config["env"][config["type"]]["ami"]["default"]
            instance_type     = "m1.average"
            frontend_network_name = "testing-net2"
            frontend_network_device = "eth1"
            backend_network_device = "eth0"
            key         = "zone1"
            key_path    = os.path.join(config["env_creds_dir"], key)
            cookbooks = filter(lambda line: len(line.strip()) > 0,
                               open("/".join((config["deployer_root"], "cookbook_list"))).read().split())
            log_dir        = config["env_log_dir"]
            resolver    = config["resolver"]
            sphinx_cache_ipaddr    = "dbfarm.ngs.local"
            env_id        = config["env_id"]
            env_is_devel    = (config["type"] == "dev")
            root_zone    = config["root_domain"]
            env_zone    = config["domain"]
            rolenames = ['webnode', 'phpnode', 'cachenode', 'memcached',
                         'search', 'dbwrite-farmed', 'dbread-farmed',
                         'mongodb-farmed', 'redis', 'task', 'storage',
                         'static', 'video-server', 'updater']
            run_lists = [['webnode', 'phpnode', 'cachenode', 'video-server',
                          'memcached', 'search', 'redis', 'task', 'storage',
                          'static', 'updater']]
            dbfarm_endpoints = config["env"][config["type"]]["dbfarm_endpoints"]
            if args.mysql:
                host, port = args.mysql[0].split(":")
                mysql_farmed = {"ipaddr": host, "meta": {"port": port}, "run_list": ["dbread", "dbwrite"], "roleids": {"dbread": 1, "dbwrite": 1}, "hostname": "stub-mysql"}
                # try to find farmed db
                farm = get_farm_for_db(host, map(itemgetter(0), dbfarm_endpoints["mysql"]))
                if farm:
                    try:
                        inst_id = find_farmed_db_by_attr(farm, ("port", str(port)))[0]
                    except Exception as e:
                        print "failed to determine mysql id", repr(e)
                        inst_id = 0
                else:
                    inst_id = 0
                status["dbinfo"].append({"ipaddr": host, "port": port, "id": inst_id, "type": "mysql"})
                mysql_inst = None
                mysql_done = True
            else:
                mysql_farmed, mysql_inst = get_database(config["db_loadbalancer"], 'mysql', args.jid, config["domain"])
                status["dbinfo"].append(mysql_inst)
                mysql_done = True
            if args.mongo:
                print "Using exsisting mongo"
                host, port = args.mongo[0].split(":")
                mongo_farmed = {"ipaddr": host, "meta": {"port": port}, "run_list": ["mongodb"], "roleids": {"mongodb": 1}, "hostname": "stub-mongo"}
                # try to find farmed db
                farm = get_farm_for_db(host, map(itemgetter(0), dbfarm_endpoints["mongo"]))
                if farm:
                    try:
                        inst_id = find_farmed_db_by_attr(farm, ("port", str(port)))[0]
                    except Exception as e:
                        print "failed to determine mongo id", repr(e)
                        inst_id = 0
                else:
                    inst_id = 0
                status["dbinfo"].append({"ipaddr": host, "port": port, "id": inst_id, "type": "mongo"})
                mongo_inst = None
                mongo_done = True
            else:
                mongo_farmed, mongo_inst = get_database(config["db_loadbalancer"], 'mongo', args.jid, config["domain"])
                status["dbinfo"].append(mongo_inst)
                mongo_done = True

        public_key = ""
        try:
            with open(key_path + ".pub") as public_key_file:
                public_key = public_key_file.read()
        except Exception as e:
            raise Exception("Failed to read public key! %s" % str(e))

        total = len(run_lists)

        default_run_list = ["server"]
        # FIXME: hack for external machines
        if not args.external_machine:
            default_run_list.append("cloud-instance")


        # log for bootstrap
        bootstrap_log = open(os.path.join(log_dir, "bootstrap.%s.log" % env_id), "w")





        if root_zone in config["env"][config["type"]].get("heaver_domains", []):
            config["env"][config["type"]]["compute_type"] = "heaver"
        ids = {}
        if args.external_machine is not None and config["type"] in ("func", "dev"):
            machine = args.external_machine[0]
            ids[machine] = {"ipaddr": machine,
                             "run_list": run_lists[0],
                             "running_on": machine}
        else:
            # start up VE's

            print "starting up %d virtual machines..." % total

            start_time = time.time()
            compute_type = config["env"][config["type"]].get("compute_type", "nova")
            if compute_type == "heaver":
                endpoint = config["env"][config["type"]]["heaver_endpoint"]
                heaver_client = client.Client(endpoint)
                # select custom image, if configured
                main_project = args.main_project
                ve_image = ""
                if args.image:
                    ve_image = args.image
                else:
                    ve_image = config["env"][config["type"]]["heaver_image"]
                ve_image = [ve_image]
                if main_project in config["sphinx_map"]:
                    sphinx_id = config["sphinx_map"][main_project]
                    mountpoint = "/data/soft/%s/var" % sphinx_id
                    sphinx_image = "sphinx-%s:%s" % (main_project, mountpoint)
                    ve_image.append(sphinx_image)
                ve_net = config["env"][config["type"]]["heaver_net"]
                for idx, rl in enumerate(run_lists):
                    ve_name = "node%d" % idx
                    ve_id = "%s.in.ngs.ru.%s" % (ve_name, env_zone)
                    ve = heaver_client.create_container(ve_id, ve_image,
                            options=dict(net=[ve_net], key=public_key))
                    started = heaver_client.start_container(ve_id)
                    try:
                        ids[ve_id] = dict(ipaddr=ve["data"]["ips"][0],
                            run_list=rl, running_on=endpoint)
                    except TypeError as err:
                        print("Error during getting id of container: {0}. "
                              "Value of ve: {1}".format(err, ve))
                        raise
                status["hosts_all"] = ids
            else:
                compute_map = config["env"][config["type"]]["compute_map"]
                default_rls = list(run_lists)

                for rl in run_lists:
                    for role in rl:
                        if role in role_instance_types.keys():
                            try:
                                spawn_host = get_balanced_compute_host(compute_map[role],
                                                                rl)
                            except KeyError:
                                spawn_host = get_balanced_compute_host(compute_map["default"],
                                                                rl)
                            #FIXME: хак для ENVIRONMENT-404
                            ami = get_ami(spawn_host, config, args, ami)
                            ve_id = spawn_ve(spawn_host, key,
                                            role_instance_types.get(role,
                                                                    instance_type),
                                            ami)
                            ids[ve_id] = { "ipaddr": "", "run_list": rl,
                                            "running_on": spawn_host }

                            default_rls.remove(rl)
                            break

                for rl in default_rls:
                    spawn_host = get_balanced_compute_host(compute_map["default"], rl)
                    #FIXME: хак для ENVIRONMENT-404
                    ami = get_ami(spawn_host, config, args, ami)
                    ve_id = spawn_ve(spawn_host, key, instance_type, ami)
                    ids[ve_id] = { "ipaddr": "", "run_list": rl,
                                    "running_on": spawn_host }

                # waiting for boot
                sys.stdout.write("waiting for boot")
                sys.stdout.flush()

                while True:

                    running = 0
                    out = call("euca-describe-instances %s" % " ".join(ids.keys())).split('\n')[:-1]
                    for line in out:
                        fields = line.split("\t")
                        if fields[0] == "INSTANCE" and fields[5] == "running":
                            running += 1

                    if running == total:
                        break

                    #print out

                    time.sleep(0.5)
                    sys.stdout.write(".")
                    sys.stdout.flush()

                print "\ndone. Elapsed %s seconds" % (time.time() - start_time)

        if config["env"][config["type"]].get("compute_type", "nova") == "nova":
            out = call("euca-describe-instances %s" % " ".join(ids.keys()))
            for line in out.split("\n")[:-1]:
                fields = line.split("\t")
                if fields[0] == "INSTANCE":
                    ids[fields[1]]["ipaddr"] = fields[3]

        # preconfigure host => run_list match

        # used role names
        roleids = {}

        # counters for <role><id> hostnames
        for name in rolenames:
            roleids[name] = 1

        hosts = {}
        hosts_by_role = {}
        for name in rolenames:
            hosts_by_role[name] = []

        for host_id, host in ids.items():
            roleids_host = {}

            for role in host["run_list"]:
                roleids_host[role] = roleids[role]
                roleids[role] += 1

            if args.external_machine:
                host["frontend_ipaddr"] = args.external_machine[0]
            elif config["env"][config["type"]].get("compute_type", "nova") == "heaver":
                host["frontend_ipaddr"] = host["ipaddr"]
            else:
                host["frontend_ipaddr"] = get_nova_network_address(host_id,
                                            frontend_network_name)
            host["roleids"] = roleids_host
            host["hostname"] = host_id
            host["meta"] = {}

            for role in host["run_list"]:
                hosts_by_role[role].append(host)

            hosts[host_id] = host

        status["hosts_all"] = hosts

        #FIXME: this is a hack. When cross-cluster authentication will be complete, this crutch should be replaced
        print "generating keys..."
        call("ssh-keygen -q -b 1024 -N '' -f %s/tmp/id_rsa.%s" % (config["deployer_root"], env_id))

        key_file = open(config["deployer_root"] + "/tmp/id_rsa.%s" % env_id)
        key_content = key_file.read()
        key_file.close()

        key_file_pub = open(config["deployer_root"] + "/tmp/id_rsa.%s.pub" %env_id)
        key_content_pub = key_file_pub.read()
        key_file_pub.close()

        os.unlink(config["deployer_root"] + "/tmp/id_rsa.%s" % env_id)
        os.unlink(config["deployer_root"] + "/tmp/id_rsa.%s.pub" % env_id)

        print "generating ssl cert..."
        try:
            ssl_cert, ssl_key = make_ssl_cert(config)
        except Exception as e:
            print "failed to generate ssl cert:", e
            raise
        config["ssl_cert"] = ssl_cert
        config["ssl_key"] = ssl_key

        # make special root frontends
        root_frontends = {}

        #FIXME: this is a hack for func/load environments

        if config["type"] in ("load", "realty", "storage"):

            # main frontend
            root_frontends["main"] =     {
                                            "host":        hosts_by_role["loadbalancer"][0],
                                            "relays":    hosts_by_role["webnode"]
                                         }
            # let lb know about his faith
            hosts_by_role["loadbalancer"][0]["meta"]["frontend_name"] = "main"
            # let fe know about their faith
            for webnode in hosts_by_role["webnode"]:
                webnode["meta"]["root_frontend"] = "main"

            # video-uploader
            root_frontends["video-server"] =     {
                                                    "host":        hosts_by_role["loadbalancer-haproxy"][0],
                                                    "relays":    hosts_by_role["video-server"]
                                                }
            hosts_by_role["loadbalancer-haproxy"][0]["meta"]["frontend_name"] = "video-server"
            for videoserver in hosts_by_role["video-server"]:
                videoserver["meta"]["root_frontend"] = "video-server"

            lb_enabled = True

        elif config["type"] in ("func", "dev"):
            root_frontends["main"] =    {
                                            "host":        hosts_by_role["webnode"][0],
                                            "relays":    hosts_by_role["webnode"]
                                        }

            hosts_by_role["webnode"][0]["meta"]["frontend_name"] = "main"
            hosts_by_role["webnode"][0]["meta"]["root_frontend"] = "main"

            root_frontends["video-server"] =     {
                                                    "host":    hosts_by_role["video-server"][0],
                                                    "relays":    hosts_by_role["video-server"]
                                                }

            hosts_by_role["video-server"][0]["meta"]["frontend_name"] = "video-server"
            hosts_by_role["video-server"][0]["meta"]["root_frontend"] = "video-server"

            lb_enabled = False


        #FIXME: this is very dirty hack for functional testing nodes. Since bootsrap is using only `hosts` variable,
        # modifying `hosts_by_role` will not affect bootstap process

        if config["type"] in ("func", "dev"):
            hosts_by_role["dbread"] = [mysql_farmed]
            hosts_by_role["dbwrite"] = [mysql_farmed]
            hosts_by_role["mongodb"] = [mongo_farmed]

        if config["type"] in ("load"):
            hosts_by_role["dbread"] = [dbread_farmed]
            hosts_by_role["dbwrite"] = [dbwrite_farmed]

        #FIXME: this is another uberdirty hack for loadtesting environments
        if config["type"] in ("realty", "storage"):
            if hosts_by_role.has_key("dbwrite55"):
                hosts_by_role["dbwrite55"][0]["meta"]["port"] = "3306"
            if hosts_by_role.has_key("dbread55"):
                for host in hosts_by_role["dbread55"]:
                    host["meta"]["port"] = "3306"

            if hosts_by_role.has_key("dbwrite"):
                hosts_by_role["dbwrite"][0]["meta"]["port"] = "3306"
            else:
                hosts_by_role["dbwrite"] = hosts_by_role["dbwrite55"]

            if hosts_by_role.has_key("dbread"):
                for host in hosts_by_role["dbread"]:
                    host["meta"]["port"] = "3306"
            else:
                hosts_by_role["dbread"] = hosts_by_role["dbread55"]



            if hosts_by_role.has_key("mongodb"):
                for host in hosts_by_role["mongodb"]:
                    host["meta"]["port"] = "27017"

        # allocate volumes for nodes and attach them
        if args.external_machine is None:
            print "allocating volumes..."
            for id, host in hosts.items():
                for role, conf in volume_demand_map.items():
                    if role in host["run_list"]:
                        # yes we need a volume
                        print "allocating volume for '%s' demand on '%s'..." % (role, id)
                        # volume should be allocated on same machine
                        vol = allocate_nova_volume(conf["size"], host["running_on"])
                        # это обычные тома, без индексов, здесь не надо проверять на vol_type
                        attach_nova_volume(id, vol)
                        host["meta"]["volumes"] = host["meta"].get("volumes", []) + [(vol, conf)]
                        print "...done, volume_id: %s" % vol

                        status["volinfo"].append(vol)

            print "...done volume allocating"
        #vol_idlog.close()
        skip_sphinx_cache = args.skip_sphinx_cache
        if (config["type"] == "func" or config["type"] == "dev") and not skip_sphinx_cache:
            if config["env"][config["type"]].get("compute_type", "nova") == "heaver":
                skip_sphinx_cache = True
            else:
                sphinx_host = hosts_by_role["search"][0]["running_on"]
                vol_type = config["compute-nodes"][sphinx_host]["volume_type"]
                if vol_type == "image":
                    # Индексы уже в образе, выделение не требуется
                    # Указываем только правильный ami (по-умолчанию стоит чистый образ)
                    skip_sphinx_cache = True
                    ami = config["env"][config["type"]]["ami"]["sphinxed"]
                else:
                    print "Allocating sphinx cache..."
                    prefetchef = xmlrpclib.ServerProxy(config["prefetch_endpoint"])
                    sphinx_vol = prefetchef.poll("sphinx-cache", sphinx_host)

                    # different sphinx type support cache
                    if vol_type == "nova":
                        attach_nova_volume(hosts_by_role["search"][0]["hostname"],
                                            sphinx_vol["id"])

                    host = hosts_by_role["search"][0]
                    host["meta"]["volumes"] = (host["meta"].get("volumes", [])
                            + [(sphinx_vol["id"],
                                {"mountpoint": "/data/soft/sphinx/var",
                                "size": sphinx_vol["size"],
                                "type": vol_type})])

                    status["volinfo"].append(sphinx_vol["id"])
                    skip_sphinx_cache = True
                    print "..done"

        if config["type"] == "load":
            skip_sphinx_cache = True


        # setup chef
        print "writing configuration into chef..."

        # prepare chef environment
        chef_environment_info = {
            "name": env_id,
            "description": "",
            "cookbook_versions": {},
            "json_class": "Chef::Environment",
            "chef_type": "environment",
            "default_attributes": {},
            "override_attributes": {}
        }

        # write cookbook version constraints into chef environment
        if args.cookbook_version:
            cookbook_version = args.cookbook_version[0]
        else:
            cookbook_version = str(config["cookbook_version_op"]) + " " + ".".join(map(str, config["cookbook_version"]))

        for cookbook in cookbooks:
            chef_environment_info["cookbook_versions"][cookbook] = cookbook_version

        query_knife("environment create %s" % env_id, json.dumps(chef_environment_info))






        # write configuration to databags
        query_databag("create %s" % env_id)

        query_databag("create %s hosts_by_role" % env_id, json.dumps({"id": "hosts_by_role", "data": hosts_by_role}))
        query_databag("create %s hosts_all" % env_id, json.dumps({"id": "hosts_all", "data": hosts}))
        query_databag("create %s root_frontends" % env_id, json.dumps({"id": "root_frontends", "data": root_frontends}))

        env_options = {
            "resolver": resolver,
            "zone":    env_zone,
            "root_zone":        root_zone,
            "owner":    config["owner"],
            "is_dev":    env_is_devel,
            "type": config["type"],
            "root_key":    key_content,
            "root_key_pub":    key_content_pub,
            "frontend_network_device": frontend_network_device,
            "backend_network_device": backend_network_device,
            "lb_enabled": lb_enabled,
            "sphinx_cache": {
                "ipaddr": sphinx_cache_ipaddr,
                "skip": skip_sphinx_cache,
            },
            "main_project": args.main_project,
            "ssl_cert": config["ssl_cert"],
            "ssl_key": config["ssl_key"]
        }
        if config["env"][config["type"]].has_key("central_sphinx"):
            env_options["central_sphinx"] = config["env"][config["type"]]["central_sphinx"]
        if not args.no_central_storage and config["env"][config["type"]].has_key("central_storage_zone"):
            env_options["central_storage_zone"] = config["env"][config["type"]]["central_storage_zone"]
        if not args.no_central_storage and config["env"][config["type"]].has_key("central_storage_ip"):
            env_options["central_storage_ip"] = config["env"][config["type"]]["central_storage_ip"]
        if not args.no_central_storage and config["env"][config["type"]].has_key("central_storage_mysql_port"):
            env_options["central_storage_mysql_port"] = config["env"][config["type"]]["central_storage_mysql_port"]


        query_databag("create %s environment" % env_id, json.dumps({"id": "environment", "data": env_options}))




        # set up dns
        print "setting up dns..."
        pdns = PowerDns(config["powerdns"])
        pdns.add_environment(env_zone, hosts, hosts_by_role, root_frontends)


        # bootstrapping
        for host_id in sorted(hosts.keys()):
            host = hosts[host_id]
            print "bootstrapping host %s (%s), roles \"%s\"" % (host["hostname"], host["ipaddr"], host["run_list"])
            while True:
                out = call("knife bootstrap -E %s -d ubuntu10.04-apt-ngs -N %s -i %s -r '%s' -p %s %s 2>&1" % (env_id, host["hostname"], key_path, ",".join(map(lambda name: "role[%s]" % name, default_run_list + host["run_list"])), args.port[0], host["ipaddr"]))
                if out.find("Failed to connect to") > 0 or out.find("EHOSTUNREACH") > 0 or out.find("Connection timed out") > 0 or out.find("ERROR: Network Error:") > 0 or out.find("ERROR: Errno::ECONNRESET") > 0:
                    print "host isn't ready, sleeping 3 secs"
                    time.sleep(3)
                else:
                    bootstrap_log.write(out)
                    if out.find("FATAL:") > 0:
                        # oops, bootstrap failed
                        print "bootstrapping failed! Host: %s" % host["ipaddr"]
                        if not args.dont_fail:
                            raise Exception("bootstrapping host %s failed!" % host["ipaddr"])
                    break
            #call("echo knife bootstrap -d ubuntu10.04-apt -i %s -r '%s' %s" % (key_path, host["run_list"] + "," + default_run_list, host["ipaddr"]))

        # run installer to deploy projects
        # TODO: fix multiple project redeploys in multi-host envs

        ssh_silent_opts = ("-o StrictHostKeyChecking=no "
                "-o PasswordAuthentication=no "
                "-o UserKnownHostsFile=/dev/null")

        can_reconfigure = (config["env"][config["type"]].get("compute_type", "nova") == "heaver"
                       and config["env"][config["type"]].get("heaver_preconfigured") == True)
        if can_reconfigure or args.reconfigure:
            # projects already deployed, just run db updates
            cmd = "/data/soft/installer.py reconfigure"
            for host in hosts.values():
                print "reconfiguring projects on host %s (%s), roles \"%s\"" % (host["hostname"],
                        host["ipaddr"], host["run_list"])
                out = call(("ssh %s -i %s root@%s '%s'") % \
                        (ssh_silent_opts, key_path, host["ipaddr"], cmd))
                # TODO: check output for errors

        elif args.init:
            # initial deploy
            cmd = "/data/soft/installer.py init"
            for host in hosts.values():
                print "deploying projects on host %s (%s), roles \"%s\"" % (host["hostname"],
                        host["ipaddr"], host["run_list"])
                out = call(("ssh %s -i %s root@%s %s") % \
                        (ssh_silent_opts, key_path, host["ipaddr"], cmd))
                # TODO: check output for errors

        if args.detach:
            # remove env's keys, so chef can re-register on rebootstrap
            cmd = ("rm /etc/chef/client.pem /etc/chef/validation.pem "
                    "/data/soft/sphinx*/check-acl-flag")
            for host in hosts.values():
                print "detaching env on host %s (%s), roles \"%s\"" % (host["hostname"],
                        host["ipaddr"], host["run_list"])
                out = call(("ssh %s -i %s root@%s %s") % \
                        (ssh_silent_opts, key_path, host["ipaddr"], cmd))

        print "Configuration complete"


    except KeyboardInterrupt as e:
        print "interrupted!"
        status["error"] = str(e)
        if mysql_inst and mysql_done:
            drop_database(config["db_loadbalancer"],mysql_inst["id"],mysql_inst["ipaddr"])
        if mongo_inst and mongo_done:
            drop_database(config["db_loadbalancer"],mongo_inst["id"],mongo_inst["ipaddr"])

    except client.RequestError as e:
        print "Cannot create container:", str(e)
        status["error"] = str(e)
        if mysql_inst and mysql_done:
            drop_database(config["db_loadbalancer"],mysql_inst["id"],mysql_inst["ipaddr"])
        if mongo_inst and mongo_done:
            drop_database(config["db_loadbalancer"],mongo_inst["id"],mongo_inst["ipaddr"])

    except Exception as e:
        print "Error occured during environment bootstrap: %s" % e
        status["error"] = str(e)
        traceback.print_exc()
        if mysql_inst and mysql_done:
            drop_database(config["db_loadbalancer"],mysql_inst["id"],mysql_inst["ipaddr"])
        if mongo_inst and mongo_done:
            drop_database(config["db_loadbalancer"],mongo_inst["id"],mongo_inst["ipaddr"])
        return status
    finally:
        return status


def wait_and_drop_instance(thread):
    thread.join()
    if not thread.err:
        try:
            thread.db_rpc_client.stop(thread.instance["info"]["id"])
            thread.db_rpc_client.free(thread.instance["info"]["id"])
        except (Exception, BaseException) as e:
            print "Failed to drop db", str(e)
        else:
            print "Dropped db %s" % thread.instance["info"]["id"]
    else:
        print "DB allocation failed:", str(thread.err)

def terminate_instances(instances):
    insts = " ".join(instances)

    call("euca-terminate-instances %s" % insts)

    while True:
        out = call("euca-describe-instances %s" % insts)
        if out == "":
            break


def terminate_heaver_instance(hostname, running_on, domain):
    heaver_client = client.Client(running_on)
    heaver_client.stop_container(hostname)
    heaver_client.destroy_container(hostname)


def detach_volumes(vollist):
    for vol in vollist:
        call("euca-detach-volume %s" % vol)


def delete_volumes(vollist):
    for vol in vollist:
        call("euca-delete-volume %s" % vol)


def kill(args, config, env_data, data):

    # detach volumes
    if len(env_data["volinfo"]) > 0:
        detach_volumes(env_data["volinfo"])

    # terminate instances
    instlist = env_data["hosts_all"].keys()

    # nova instances
    nova_instlist = filter(lambda inst: inst.find("i-") == 0, instlist)
    if len(nova_instlist) > 0:
        terminate_instances(nova_instlist)

    # heaver instances
    heaver_instlist = filter(lambda inst: inst[0].startswith("node"),
        env_data["hosts_all"].items())
    for hostname, instance in env_data["hosts_all"].items():
        if hostname.startswith("node"):
            try:
                terminate_heaver_instance(hostname, instance["running_on"], config["domain"])
            except client.RequestError as e:
                print "Failed to destroy container '%s', do it yourself" % hostname
                print e

    # delete volumes
    if len(env_data["volinfo"]) > 0:
        delete_volumes(env_data["volinfo"])

    # delete chef node entities
    for inst in instlist:
        query_knife("node delete -y %s" % inst)
        query_knife("client delete -y %s" % inst)

    # delete chef databags
    query_knife("data bag delete -y %s" % config["env_id"])

    # delete chef environment
    query_knife("environment delete -y %s" % config["env_id"])

    # clean up dns
    pdns = PowerDns(config["powerdns"])
    pdns.remove_environment(config["domain"])

    # get all used ids
    another_data = data.copy()
    another_data.pop(config["env_id"])

    dbs_f = dict()
    for env in another_data.values():
        for db in env["databases"]:
            dbf = "http://"+db["ipaddr"]+":8000"

            if dbf not in dbs_f.keys():
                dbs_f.update({dbf : list()})
            dbs_f[dbf].append(db["id"])

    e_type = data[args.domain[0].replace('.','-')]['type']

    print "Dropping dbs"
    for db in env_data["dbinfo"]:
        if db["id"] == 0:
            print "Skipping database %(ipaddr)s:%(port)s" % db
            continue

        if args.save_mysql and db["type"] == "mysql":
            print "Skipping %(type)s database %(ipaddr)s:%(port)s" % db
            continue

        if args.save_mongo and db["type"] == "mongo":
            print "Skipping %(type)s database %(ipaddr)s:%(port)s" % db
            continue

        if dbs_f.get("http://%(ipaddr)s:8000" % db, []).count(db["id"]) > 0:
            print "Skipping shared %(type)s database %(ipaddr)s:%(port)s" % db
            continue

        try:
            drop_database(config["db_loadbalancer"], db["id"], db["ipaddr"])
        except:
            print ("Error occured while removing farmed db: "
                   "{0[ipaddr]}:{0[port]}, id {0[id]}\n{1}".format(
                       db, traceback.format_exc()))

    return True
