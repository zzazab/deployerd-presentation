package main

import "os"

func main() {
	args := libprovider.ParseArgs()
	worker := Provider{}
	libprovider.ReadProfile(
		os.Stdin,
		libprovider.Profile{
			Resource:  &worker.resource,
			Provider:  &worker.commonParams,
			Meta:      &worker.meta,
			Allocated: &workere.allocated,
		},
	)

	libprovider.Run(&worker, args)
}
